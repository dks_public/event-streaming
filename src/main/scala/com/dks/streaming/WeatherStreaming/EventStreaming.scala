package com.dks.streaming.WeatherStreaming

/** Spark Streaming application which collects events from streaming source (Kafka) in the form of JSON parses it and
  * Store them in HBase table.
  */

import com.typesafe.config.{Config, ConfigFactory}
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.streaming.kafka010.{CanCommitOffsets, HasOffsetRanges, KafkaUtils}
import org.json.simple.{JSONArray, JSONObject}
import org.json.simple.parser.JSONParser
import org.apache.hadoop.hbase.HBaseConfiguration
import org.apache.hadoop.hbase.client.{Connection, ConnectionFactory, Put}
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.spark.SparkConf
import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe
import org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent
import org.apache.hadoop.hbase.TableName


object EventStreaming {

  /**Initialize Hbase Configuration*/
  val conf = HBaseConfiguration.create
   conf.addResource("hbase-site.xml")


  def main(args:Array[String]): Unit = {

    /**Check checkpoint directory to get the streaming context or create a new one*/
    val ssc = StreamingContext.getOrCreate(getConfigProperties("spark").getString("checkpoint"),  createStreamingContext)
   ssc.start()
   ssc.awaitTermination()
  }

  /** Helper method to load configurations */
  def getConfigProperties( key: String):Config = {

    val config = ConfigFactory.load()
    config.getConfig(key)
  }

  /** Function to Initialize streamingContext */
  def initializeStreamingContext:StreamingContext ={

    val sparkConfig = getConfigProperties("spark")
    val conf = new SparkConf()
      .setMaster("local[*]").set("mapreduce.fileoutputcommitter.marksuccessfuljobs",sparkConfig.getString("markSuccessfulJobs")).
      set("spark.scheduler.mode", sparkConfig.getString("schedulerMode")).
      set("spark.streaming.stopGracefullyOnShutdown",sparkConfig.getString("gracefulShutDown")).setAppName(sparkConfig.getString("appName"))
    new StreamingContext(conf, Seconds(sparkConfig.getInt("microBatchTime")))

  }

  /** Function for creating/reusing same spark context for SparkContext get or create function*/
  def createStreamingContext():StreamingContext ={

    val ssc = initializeStreamingContext
    pipelineRunner(ssc)
    ssc.checkpoint(getConfigProperties("spark").getString("checkpoint"))
    ssc
  }


  /**Function to initialize KafkaDstreams*/
  def getKafkaStream(streamingContext:StreamingContext) ={

    val kafkaConfig = getConfigProperties("kafka")

    val kafkaParams = Map[String, Object](
      "bootstrap.servers" -> kafkaConfig.getString("bootstrapServers"),
      "key.deserializer" -> classOf[StringDeserializer],
      "value.deserializer" -> classOf[StringDeserializer],
      "group.id" -> kafkaConfig.getString("groupId"),
      "auto.offset.reset" -> "latest",
      "enable.auto.commit" -> (false: java.lang.Boolean)
    )

    val topics = Array(kafkaConfig.getString("topic1"))
    KafkaUtils.createDirectStream[String, String](streamingContext, PreferConsistent, Subscribe[String, String](topics, kafkaParams)
    )
  }

  /** Function which holds business logic */
  def pipelineRunner(streamingContext: StreamingContext):Unit = {

    val stream = getKafkaStream(streamingContext)

    stream.foreachRDD(rdd => {

      if (!rdd.isEmpty()) {

        val offsetRanges = rdd.asInstanceOf[HasOffsetRanges].offsetRanges


        rdd.foreachPartition(iter => {

          /** Opening Hbase Connection at partition level*/
          val connection:Connection = ConnectionFactory.createConnection(conf)

          val parser = new JSONParser

          iter.foreach(record => {

            /**  sample value
              * """{
              * "OrderId":"OD123",
              * "Date":"2016-05-20"
              * "Items" : [
              * {"ItemId" : "12312", "Price" : 45,"Quantity"1 },
              * {"ItemId" : "12313", "Price" : 40,"Quantity":1 },
              * {"ItemId" : "12313", "Price" : 30,"Quantity":1 }
              *
              * ],
              * "Total":115
              *
              * }"""
              */
            val  jsonOBJ = parser.parse(record.value()).asInstanceOf[JSONObject]

           val table = connection.getTable(TableName.valueOf("nms:table_name"))

            val rowKey=jsonOBJ.get("OrderId").asInstanceOf[String]
            val put = new Put(rowKey.getBytes)

            /**Adding attributes to the Data family  like date and total amount*/
            put.addColumn("Data".getBytes(),"Date".getBytes(),jsonOBJ.get("Date").asInstanceOf[String].getBytes())
            put.addColumn("Data".getBytes(),"Total".getBytes(),jsonOBJ.get("Total").asInstanceOf[Long].toString.getBytes())

            /** Json array object which holds the values of items*/
            val jsonArr = jsonOBJ.get("Items").asInstanceOf[JSONArray]

            /** Iterating through json array and appending all the different items with put object
            column qualifier is itemId combined with the respective attribute like price, quantity seperated with '|' */
            for(i <- 0 until jsonArr.size() - 1){
              val items = jsonArr.get(i).asInstanceOf[JSONObject]
              val itemId = items.get("ItemId").asInstanceOf[String]
              val price  = items.get("Price").asInstanceOf[Long].toString
              val quantity = items.get("Quantity").asInstanceOf[Long].toString
              put.addColumn("Items".getBytes(),itemId.concat("|ItemID").getBytes(),itemId.getBytes())
              put.addColumn("Items".getBytes(),itemId.concat("|Price").getBytes(),price.getBytes())
              put.addColumn("Items".getBytes(),itemId.concat("|Quantity").getBytes(),quantity.getBytes())
            }

            /** Add kafka metadata for traceability */
            put.addColumn("msgmeta".getBytes(),"Offset".getBytes(),record.offset().toString.getBytes())
            put.addColumn("msgmeta".getBytes(),"Partition".getBytes(),record.partition().toString.getBytes())
            put.addColumn("msgmeta".getBytes(),"timestamp".getBytes(),record.timestamp().toString.getBytes())

            table.put(put)
            table.close();

          })

          /**close the connection*/
          connection.close()
        })

        /**commit the offsets*/
        stream.asInstanceOf[CanCommitOffsets].commitAsync(offsetRanges)

      }
    })

  }



  }
