# Event Streaming 

**Retail Events**

Retail Events are generated when user performs certain actions like placing orders, cancelling items, raising tickets and completing transactions.
Managing all these events are critical for a retailer as he can enrich the user experience based on the events and for further analysis.

**Design**

As for this application is concerened, the retail event is placing orders. Once the user places an order the event will be publiched to Kafka. Spark
(Dstreams) consumes those events, parses it and then insert them into HBase table.



